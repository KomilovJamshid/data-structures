package uz.jamshid.linearDataStructures.stack;

import java.util.Scanner;
import java.util.Stack;

public class ReversingString {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.print("Enter your name: ");
        String str = scanner.nextLine();
        Stack<String> stack = new Stack<>();
        for (int i=0; i<str.length(); ++i) {
            stack.push(str.substring(i,i+1));
        }
        System.out.println(stack);
        Stack<String> reverseStack=new Stack<>();
        for (int i=0; i<str.length(); ++i) {
            String top=stack.pop();
            reverseStack.push(top);
        }
        System.out.println(reverseStack);
    }
}
