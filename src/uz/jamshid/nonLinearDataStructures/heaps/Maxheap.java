package uz.jamshid.nonLinearDataStructures.heaps;

public class Maxheap {
    public static void heapify(int[] array) {
        int lastParentIndex = array.length / 2 - 1;
        for (int i = lastParentIndex; i >= 0; i--)
            heapify(array, i);
    }

    private static void heapify(int[] array, int index) {
        int largerIndex = index;

        int leftIndex = index * 2 + 1;
        if (leftIndex < array.length &&
                array[leftIndex] > array[largerIndex])
            largerIndex = leftIndex;

        int rightindex = index * 2 + 2;
        if (rightindex < array.length &&
                array[rightindex] > array[largerIndex])
            largerIndex = rightindex;

        if (index == largerIndex)
            return;

        swap(array, index, largerIndex);
        heapify(array, largerIndex);
    }

    private static void swap(int[] array, int first, int second) {
        int temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }

    public static int getkthLargestElement(int[] array, int k) {
        if (k < 1 && k > array.length)
            throw new IllegalArgumentException();

        Heap heap = new Heap();
        for (int number : array)
            heap.insert(number);

        for (int i = 0; i < k; i++)
            heap.remove();

        return heap.max();
    }
}
